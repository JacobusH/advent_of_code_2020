def go():
    filepath = './inputs/day_2'
    with open(filepath) as fp:
        line = fp.readline()
        cnt = 1
        while line:
            letter_range, target_letter, sequence = line.split(' ')
            letter_range_1, letter_range_2 = letter_range.split('-')
            target_letter = target_letter[0]

            print("Line {}: {}".format(cnt, line.strip()))
            print("range_1 {}.. range_2 {}... target {}... password {}\n".format(letter_range_1, letter_range_2, target_letter, sequence))
            
            line = fp.readline()
            cnt += 1

if __name__ == '__main__':
    go()